from telebot import types
from core.callbacks import *
from navigation_microservice.texts import *


def navigation_markup():
    keyboard = types.InlineKeyboardMarkup()
    city_button = types.InlineKeyboardButton(text=CITY_BTN_TXT, callback_data=city_callback)
    area_button = types.InlineKeyboardButton(text=AREA_BTN_TXT, callback_data=area_callback)
    keyboard.add(city_button, area_button)
    return keyboard
