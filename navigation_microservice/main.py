import logging

import telebot
from telebot import apihelper
from telebot import types as t

from core.callbacks import city_callback, area_callback
from core.consumer import PickleConsumer
from core.database import update_state, save_statistic
from core.texts import *
from navigation_microservice.database import *
from navigation_microservice.markups import navigation_markup
from navigation_microservice.texts import *


def callback(consumer, body):
    if hasattr(body, 'data'):
        bot.process_new_callback_query([body])
    else:
        bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'navigation', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='navigation')

# Logger
logger = logging.getLogger(__name__)


@bot.message_handler(regexp=NAVIGATION_EMOJI)
def choice(message: t.Message):
    update_state(message.chat.id, 1)
    save_statistic(message.chat.id, c.NAVIGATION)
    bot.send_message(message.chat.id, NAVIGATION_QUESTION, reply_markup=navigation_markup())


@bot.callback_query_handler(func=lambda c: c.data == area_callback)
def area(message: t.CallbackQuery):
    tg_id = message.message.chat.id
    msg = get_area_id(tg_id)
    bot.delete_message(tg_id, message.message.message_id)
    if msg is not None:

        if is_area_picture():
            bot.send_photo(tg_id, msg, caption=AREA_MSG_TXT)
        else:
            bot.send_document(tg_id, msg, caption=AREA_MSG_TXT)

    else:
        text = get_area_text(tg_id)
        if text:
            bot.send_message(tg_id, text)
        else:
            bot.send_message(tg_id, INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, NAVIGATION_QUESTION, reply_markup=navigation_markup())


@bot.callback_query_handler(func=lambda c: c.data == city_callback)
def city(message: t.CallbackQuery):
    tg_id = message.message.chat.id
    msg = get_city_id(tg_id)
    bot.delete_message(tg_id, message.message.message_id)
    if msg is not None:

        if is_city_picture():
            bot.send_photo(tg_id, msg, caption=CITY_MSG_TXT)
        else:
            bot.send_document(tg_id, msg, caption=CITY_MSG_TXT)

    else:
        text = get_city_text(tg_id)
        if text:
            bot.send_message(tg_id, text)
        else:
            bot.send_message(tg_id, INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, NAVIGATION_QUESTION, reply_markup=navigation_markup())


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[NAVIGATION] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
