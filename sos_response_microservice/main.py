import logging

import telebot
from telebot import apihelper

from core import settings as s
from core.consumer import PickleConsumer


def callback(consumer, body):
    sender([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'sos_response', callback, s.RABBITMQ_USERNAME,
                     s.RABBITMQ_PASSWORD, exchange='info_bot', logger_name='sos_response')

# Logger
logger = logging.getLogger(__name__)


def sender(body):
    bot.send_message(body[0][0], body[0][1])


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[SOS RESPONSE] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
