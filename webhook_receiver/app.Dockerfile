FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

EXPOSE 443
EXPOSE 8443

ADD  ./core/ /src/core/

ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./webhook_receiver/ /src/webhook_receiver/
CMD [ "python", "./webhook_receiver/app.py" ]
