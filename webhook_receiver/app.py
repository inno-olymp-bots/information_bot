#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import cherrypy as cherrypy
import telebot
from core import database
from core.database import is_key_valid, register_chat
from core.publisher import PicklePublisher
from core.settings import *
from core.texts import *
import cheroot.ssl.builtin
cheroot.ssl.builtin.IS_BELOW_PY37 = True


logger = logging.getLogger(__name__)

bot = telebot.TeleBot(TOKEN, num_threads=8)

queues = ['registration', 'table', 'SOS', 'menu', 'navigation', 'programs', 'participant', 'viewer']
pub = PicklePublisher(RABBITMQ_HOST, RABBITMQ_PORT, queues, RABBITMQ_USERNAME, RABBITMQ_PASSWORD, exchange='info_bot',
                      logger_name='webhook')


class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                'content-type' in cherrypy.request.headers and \
                cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)


@bot.callback_query_handler(
    func=lambda call: call.data.startswith('reg=') or (database.get_state(call.message.chat.id) <= 0))
@bot.message_handler(func=lambda m: (m.chat.type == "private") and (database.get_state(m.chat.id) <= 0))
def registration(message):
    logger.log(logging.INFO, 'Queue: ' + 'registration')
    pub.publish_message(
        message,
        'registration'
    )


@bot.message_handler(regexp=TABLE_EMOJI)
@bot.callback_query_handler(func=lambda c: c.data.startswith('tab='))
def table(message):
    logger.log(logging.INFO, 'Queue: ' + 'table')

    pub.publish_message(
        message,
        'table'
    )


@bot.message_handler(regexp=NAVIGATION_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('navi='))
def navigation(message):
    logger.log(logging.INFO, 'Queue: ' + 'navigation')
    pub.publish_message(
        message,
        'navigation'
    )


@bot.message_handler(regexp=PROGRAM_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('prog='))
def programs_schedule(message):
    logger.log(logging.INFO, 'Queue: ' + 'programs')
    pub.publish_message(
        message,
        'programs'
    )


@bot.message_handler(regexp=PARTICIPANT_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('part='))
def programs_schedule(message):
    logger.log(logging.INFO, 'Queue: ' + 'participant')
    pub.publish_message(
        message,
        'participant'
    )


@bot.message_handler(regexp=VIEWER_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('view='))
def viewer(message):
    logger.log(logging.INFO, 'Queue: ' + 'viewer')
    pub.publish_message(
        message,
        'viewer'
    )


@bot.message_handler(
    func=lambda m: (m.chat.type == "private") and (database.get_state(m.chat.id) == 2) or (SOS_EMOJI == m.text))
def sos(message):
    logger.log(logging.INFO, 'Queue: ' + 'SOS')
    pub.publish_message(
        message,
        'SOS'
    )


@bot.message_handler(func=lambda m: m.chat.type in ["group", "supergroup", ])
def group(message):
    logger.log(logging.INFO, 'Queue: ' + 'group')
    msg = message.text.split(" ")
    if msg[0] == '/key':
        if is_key_valid(message.text):
            key = msg[1]
            register_chat(message.chat.id, key)
            bot.send_message(message.chat.id, GROUP_REGISTERED)
        else:
            bot.send_message(message.chat.id, GROUP_KEY_INVALID)


@bot.message_handler(func=lambda m: m.chat.type == "private")
def main_menu(message):
    logger.log(logging.INFO, 'Queue: ' + 'menu')
    pub.publish_message(
        message,
        'menu'
    )


bot.remove_webhook()

time.sleep(0.1)

bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))

cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})

try:
    pub.run()
    cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})
except KeyboardInterrupt:
    pub.terminate()
