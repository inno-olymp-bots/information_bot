from viewer_microservice.texts import *
from telebot import types
from core import texts as t
from core import callbacks as cb


def viewer_markup():
    keyboard = types.InlineKeyboardMarkup()
    transport_button = types.InlineKeyboardButton(text=TRANSPORT_BTN_TXT, callback_data=cb.viewer_transport_callback)
    auth_button = types.InlineKeyboardButton(text=COMFORT_BTN_TXT, callback_data=cb.viewer_auth_callback)

    keyboard.add(transport_button)
    keyboard.add(auth_button)
    return keyboard


def transport_markup():
    keyboard = types.InlineKeyboardMarkup()
    shuttle_button = types.InlineKeyboardButton(text=SHUTTLE_BTN_TXT, callback_data=cb.viewer_shuttle_callback)
    taxi_button = types.InlineKeyboardButton(text=TAXI_BTN_TXT, callback_data=cb.viewer_taxi_callback)
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_viewer_callback)

    keyboard.add(shuttle_button, taxi_button)
    keyboard.add(back_button)
    return keyboard


def auth_agree_markup():
    keyboard = types.InlineKeyboardMarkup()
    agree_button = types.InlineKeyboardButton(text=AGREE_BTN_TXT, callback_data=cb.confirm_callback)
    cancel_button = types.InlineKeyboardButton(text=CANCEL_BTN_TXT, callback_data=cb.back_viewer_callback)

    keyboard.add(agree_button, cancel_button)
    return keyboard


def transport_days_markup():
    keyboard = types.InlineKeyboardMarkup()
    first_day_button = types.InlineKeyboardButton(text=FIRST_DAY_BTN_TXT, callback_data=cb.viewer_shuttle_1_callback)
    second_day_button = types.InlineKeyboardButton(text=SECOND_DAY_BTN_TXT, callback_data=cb.viewer_shuttle_2_callback)
    third_day_button = types.InlineKeyboardButton(text=THIRD_DAY_BTN_TXT, callback_data=cb.viewer_shuttle_3_callback)
    back_button = types.InlineKeyboardButton(text=t.BACK, callback_data=cb.back_viewer_callback)

    keyboard.add(first_day_button, second_day_button, third_day_button)
    keyboard.add(back_button)
    return keyboard
