import psycopg2
from core import settings as s
from core import configs as c


def get_shuttle(tg_id, day):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()

    cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
    user = cursor.fetchall()[0]
    status, token = user[0], user[1]
    has_token = True if (token != "") and (token is not None) else False
    cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                   f' ({tg_id}, {c.VIEWER_TRANSPORT}, now(), {status}, {has_token})')
    connection.commit()

    try:

        cursor.execute(f'SELECT region_id FROM users WHERE tg_id={tg_id}')
        region_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT transport_id FROM users_region WHERE id={region_id}')
        transport_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT shuttle_{day} FROM info_transport WHERE id={transport_id}')
        shuttle = cursor.fetchall()[0][0]
        connection.close()
        if shuttle == "":
            return None
        return shuttle
    except:
        connection.close()
        return None


def get_taxi(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()

    cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
    user = cursor.fetchall()[0]
    status, token = user[0], user[1]
    has_token = True if (token != "") and (token is not None) else False
    cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                   f' ({tg_id}, {c.VIEWER_TRANSPORT}, now(), {status}, {has_token})')
    connection.commit()

    try:
        cursor.execute(f'SELECT region_id FROM users WHERE tg_id={tg_id}')
        region_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT transport_id FROM users_region WHERE id={region_id}')
        transport_id = cursor.fetchall()[0][0]
        cursor.execute(f'SELECT taxi FROM info_transport WHERE id={transport_id}')
        taxi = cursor.fetchall()[0][0]
        connection.close()
        if taxi == "":
            return None
        return taxi
    except:
        connection.close()
        return None
