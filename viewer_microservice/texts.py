QUESTION = "Выберите пункт"
TRANSPORT_QUESTION = "Выберите расписание"

TRANSPORT_BTN_TXT = "Транспорт"
COMFORT_BTN_TXT = "Хочу удобно!"

SHUTTLE_BTN_TXT = "Шаттлы"
TAXI_BTN_TXT = "Такси"

WARNING_MESSAGE = "В этом разделе вы можете сменить регистрационные данные," \
                  " чтобы удобней смотреть результаты соревнований и получать информацию для вашего региона и статуса." \
                  "Вы согласны обновить данные о себе? Вы все еще сможете войти по коду участника."
AGREE_BTN_TXT = "Я согласен"
CANCEL_BTN_TXT = "Отмена"

CHOOSE_DAY = "Выберите день"
FIRST_DAY_BTN_TXT = "22 июня"
SECOND_DAY_BTN_TXT = "23 июня"
THIRD_DAY_BTN_TXT = "24 июня"
