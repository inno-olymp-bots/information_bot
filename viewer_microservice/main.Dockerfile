FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./viewer_microservice/ /src/viewer_microservice/
CMD [ "python", "./viewer_microservice/main.py" ]
