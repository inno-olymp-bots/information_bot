import logging

import telebot
from telebot import apihelper
from telebot import types as ty

import core.database as db
import core.texts as tx
from core.consumer import PickleConsumer
from core import callbacks as cb

from viewer_microservice.database import *
from viewer_microservice.texts import *
from viewer_microservice import markups as mk


def callback(consumer, body):
    if hasattr(body, 'data'):
        bot.process_new_callback_query([body])
    else:
        bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'viewer', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='viewer')

# Logger
logger = logging.getLogger(__name__)



@bot.message_handler(regexp=tx.VIEWER_EMOJI)
def choice(message: ty.Message):
    db.update_state(message.chat.id, 1)
    db.save_statistic(message.chat.id, c.VIEWER)
    bot.send_message(message.chat.id, QUESTION, reply_markup=mk.viewer_markup())


@bot.callback_query_handler(func=lambda call: call.data == cb.back_viewer_callback)
def back(call):
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=QUESTION,
        reply_markup=mk.viewer_markup()
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.viewer_transport_callback)
def transport(call):
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=TRANSPORT_QUESTION,
        reply_markup=mk.transport_markup()
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.viewer_auth_callback)
def auth(call):
    user_id = db.get_id_by_tg_id(call.message.chat.id)
    logger.log(logging.CRITICAL, user_id)
    auth_info = db.get_user_info(user_id)
    user_name = db.get_user_name(call.message.chat.id)
    text = tx.USER_INFO.format(user_name, tx.STATUS_CHOICES[str(auth_info['status'])])
    if auth_info['region'] is not None:
        text += tx.REGION_INFO.format(auth_info['region'])
    if auth_info['competition'] is not None:
        text += tx.COMPETITION_INFO.format(auth_info['competition'])
    text += WARNING_MESSAGE
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=text,
        reply_markup=mk.auth_agree_markup(),
        parse_mode='HTML'
    )


@bot.callback_query_handler(func=lambda call: call.data in [cb.viewer_taxi_callback, cb.viewer_shuttle_callback])
def transport_info(call):
    tg_id = call.message.chat.id
    if call.data == cb.viewer_taxi_callback:
        bot.delete_message(tg_id, call.message.message_id)
        taxi = get_taxi(tg_id)
        if taxi is not None:
            bot.send_message(tg_id, taxi)
        else:
            bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

        bot.send_message(tg_id, TRANSPORT_QUESTION, reply_markup=mk.transport_markup())

    else:
        bot.edit_message_text(
            chat_id=tg_id,
            message_id=call.message.message_id,
            text=CHOOSE_DAY,
            reply_markup=mk.transport_days_markup()
        )


@bot.callback_query_handler(
    func=lambda call: call.data in [cb.viewer_shuttle_1_callback, cb.viewer_shuttle_2_callback,
                                    cb.viewer_shuttle_3_callback])
def shuttle_schedule(call):
    tg_id = call.message.chat.id
    bot.delete_message(tg_id, call.message.message_id)

    if call.data == cb.viewer_shuttle_1_callback:
        schedule = get_shuttle(tg_id=tg_id, day=1)
    elif call.data == cb.viewer_shuttle_2_callback:

        schedule = get_shuttle(tg_id=tg_id, day=2)
    else:
        schedule = get_shuttle(tg_id=tg_id, day=3)

    if schedule is not None and schedule != "":
        bot.send_message(tg_id, schedule)
    else:
        bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, CHOOSE_DAY, reply_markup=mk.transport_days_markup())


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[VIEWER] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
