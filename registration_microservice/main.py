import logging

import telebot
from telebot import apihelper

from core.database import *
from core import settings as s
from core.markups import main_menu_markup
from core.texts import STATUS_CHOICES
from core.consumer import PickleConsumer
import core.callbacks as cb

from registration_microservice import database
from registration_microservice import texts as t
from registration_microservice import markups as m


def callback(consumer, body):
    if hasattr(body, 'data'):
        bot.process_new_callback_query([body])
    else:
        bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'registration', callback, s.RABBITMQ_USERNAME,
                     s.RABBITMQ_PASSWORD, exchange='info_bot', logger_name='registration')

# Logger
logger = logging.getLogger(__name__)


def filter_code(text):
    text = str(text).strip()
    if text.isnumeric():
        return text
    else:
        return '-1'


def get_region_codes():
    text = ''
    for row in database.get_region_names_codes():
        text += '<b>' + str(row[0]) + '</b> - ' + row[1] + '\n'
    return text


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.handle_answer_callback))
def handle_answer(call):
    data = call.data.split('=')
    chat = call.message.chat
    next_user_id = database.get_next_user_id(chat.id)
    if data[2] == 'yes':
        update_state(chat.id, 0)
        database.reset_tg_id(chat.id)
        user_id = database.get_id_by_old_tg_id(chat.id)
        database.register_user_with_status(user_id=user_id,
                                           user_name=chat.first_name + ' ' + chat.last_name,
                                           tg_id=chat.id,
                                           alias=chat.username,
                                           status=7)  # status = Viewer
        if user_id is None:
            user_id = get_id_by_tg_id(chat.id)
        database.move_data(user_id, next_user_id)
        text = t.SUCCESSFULLY_REGISTERED
    else:
        text = t.REGISTRATION_DATA_LEFT
    database.delete_next_user_by_id(next_user_id)
    update_state(chat.id, 1)
    bot.delete_message(chat_id=chat.id,
                       message_id=call.message.message_id)
    bot.send_message(chat_id=chat.id,
                     text=text,
                     reply_markup=main_menu_markup())


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.reg_competition_callback))
def update_competition(call):
    data = call.data.split('=')
    chat = call.message.chat
    next_user_id = database.get_next_user_id(chat.id)
    database.set_competition_by_id(next_user_id, data[2])
    user_info = get_user_info(next_user_id)
    bot.edit_message_text(chat_id=chat.id,
                          message_id=call.message.message_id,
                          text=t.VERIFY_PERSONAL_INFO.format(
                              STATUS_CHOICES[str(user_info['status'])],
                              user_info['region']) + t.VERIFY_COMPETITION.format(
                              user_info['competition']),
                          reply_markup=m.verify_info_markup(),
                          parse_mode='HTML')


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.reg_category_callback))
def handle_competition_category(call):
    data = call.data.split('=')
    chat = call.message.chat
    bot.edit_message_text(chat_id=chat.id,
                          message_id=call.message.message_id,
                          text=t.CHOOSE_COMPETITION,
                          reply_markup=m.choose_competition_markup(data[2]))


@bot.message_handler(func=lambda message: get_state(message.chat.id) == -1)
def handle_region_code(message):
    code = filter_code(message.text)
    next_user_id = database.get_next_user_id(message.chat.id)
    if database.set_user_region(next_user_id, code):
        update_state(message.chat.id, -2)
        user_info = get_user_info(next_user_id)
        if database.get_status_by_id(next_user_id) in [3, 5]:  # see core.texts.STATUS_CHOICES
            bot.send_message(chat_id=message.chat.id,
                             text=t.VERIFY_PERSONAL_INFO.format(STATUS_CHOICES[str(user_info['status'])],
                                                                user_info['region']),
                             reply_markup=m.verify_info_markup(),
                             parse_mode='HTML')
        else:
            bot.send_message(chat_id=message.chat.id,
                             text=t.CHOOSE_CATEGORY,
                             reply_markup=m.choose_category_markup(user_info['status']))
    else:
        bot.send_message(chat_id=message.chat.id,
                         text=t.WRONG_REGION_CODE)


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.status_callback))
def choose_region(call):
    data = call.data.split('=')
    chat = call.message.chat
    next_user_id = database.get_next_user_id(chat.id)
    database.set_next_user_status(user_id=next_user_id,
                                  status=data[2])
    text = get_region_codes()
    update_state(chat.id, t.REGION_CODE_INPUT_STATE)
    bot.edit_message_text(chat_id=chat.id,
                          message_id=call.message.message_id,
                          text=t.CHOOSE_REGION + text,
                          parse_mode='HTML')


@bot.callback_query_handler(func=lambda call: call.data == cb.confirm_callback)
def choose_status(call):
    chat = call.message.chat
    update_state(chat.id, -2)
    database.register_next_user(tg_id=chat.id,
                                user_name=chat.first_name + ' ' + chat.last_name,
                                alias=chat.username)
    bot.edit_message_text(chat_id=chat.id,
                          message_id=call.message.message_id,
                          text=t.CHOOSE_STATUS,
                          reply_markup=m.choose_status_markup())


@bot.callback_query_handler(func=lambda call: call.data == cb.auth_accept_callback)
def authorization(call):
    tg_id = call.message.chat.id
    update_state(tg_id, 0)
    bot.edit_message_text(chat_id=tg_id,
                          message_id=call.message.message_id,
                          text=t.ASK_TO_SEND_TOKEN,
                          reply_markup=m.get_token_markup())


@bot.callback_query_handler(
    func=lambda call: call.data == cb.no_token_callback and get_state(call.message.chat.id) == 0)
def no_token(call):
    chat = call.message.chat
    if tg_id_exists(chat.id):
        text = t.REGISTRATION_DATA_LEFT
    else:
        user_id = database.get_id_by_old_tg_id(chat.id)
        database.register_user_with_status(user_id=user_id,
                                           user_name=str(chat.first_name) + ' ' + str(chat.last_name),
                                           tg_id=chat.id,
                                           alias=chat.username,
                                           status=7)  # status = Viewer
        text = t.MENU_TEXT
    update_state(chat.id, 1)
    bot.delete_message(chat_id=chat.id,
                       message_id=call.message.message_id)
    bot.send_message(chat_id=chat.id,
                     text=text,
                     reply_markup=main_menu_markup())


@bot.callback_query_handler(func=lambda call: get_state(call.message.chat.id) == 0)
def callback_unregistered(call):
    bot.send_message(chat_id=call.message.chat.id,
                     text=t.ASK_TO_SEND_TOKEN,
                     reply_markup=m.get_token_markup())


@bot.message_handler(func=lambda message: get_state(message.chat.id) == 0)
def registration(message):
    words = str.split(message.text)
    user_id = message.chat.id
    if len(words) == 1 and words[0] == '/start':
        bot.send_message(chat_id=user_id,
                         text=t.ASK_TO_SEND_TOKEN,
                         reply_markup=m.get_token_markup())
    elif len(words) <= 2:
        if len(words) == 1:
            token = words[0]
        else:
            token = words[1]
        if database.find_token(tg_id=user_id, user_token=token):
            bot.send_message(chat_id=user_id,
                             text=t.SUCCESSFULLY_REGISTERED,
                             reply_markup=main_menu_markup())
            database.add_alias(user_id, message.chat.username)
        else:
            bot.send_message(chat_id=user_id,
                             text=t.WRONG_TOKEN,
                             reply_markup=m.get_token_markup())
    # wrong format of msg
    else:
        bot.send_message(chat_id=user_id,
                         text=t.WRONG_MESSAGE_FORMAT,
                         reply_markup=m.get_token_markup())


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[REGISTRATION] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
