import telebot
import registration_microservice.texts as t
import core.callbacks as cb
from core.texts import STATUS_CHOICES
from registration_microservice import database


def get_token_markup():
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(telebot.types.InlineKeyboardButton(text=t.NO_TOKEN_BUTTON, callback_data=cb.no_token_callback))
    return keyboard


def choose_status_markup():
    keyboard = telebot.types.InlineKeyboardMarkup()
    for index in STATUS_CHOICES.keys():
        keyboard.add(
            telebot.types.InlineKeyboardButton(text=STATUS_CHOICES[index],
                                               callback_data=cb.status_callback + f'{index}'))
    return keyboard


def choose_category_markup(user_status):
    keyboard = telebot.types.InlineKeyboardMarkup()
    for cat in database.get_categories_full_names_ids():
        if not (user_status == 4 and cat['id'] == 3):
            keyboard.add(telebot.types.InlineKeyboardButton(text=cat['cat_name'],
                                                            callback_data=cb.reg_category_callback + f"{cat['id']}"))
    return keyboard


def choose_competition_markup(cat_id):
    keyboard = telebot.types.InlineKeyboardMarkup()
    for comp in database.get_competition_full_names_ids(cat_id):
        keyboard.add(telebot.types.InlineKeyboardButton(text=comp['comp_name'],
                                                        callback_data=cb.reg_competition_callback + f"{comp['id']}"))
    return keyboard


def verify_info_markup():
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(telebot.types.InlineKeyboardButton(text=t.YES, callback_data=cb.handle_answer_callback + 'yes'))
    keyboard.add(telebot.types.InlineKeyboardButton(text=t.NO, callback_data=cb.handle_answer_callback + 'no'))
    return keyboard
