from core.texts import *

ASK_TO_SEND_TOKEN = "Пожалуйста, отправьте ваш токен!"
SUCCESSFULLY_REGISTERED = f"Вы успешно зарегистрировались!\n{MENU_TEXT}"

WRONG_TOKEN = "Вы отправили неверный токен! Попробуйте еще раз."
WRONG_REGION_CODE = 'Вы отправили неверный код региона! Попробуйте еще раз.'
WRONG_MESSAGE_FORMAT = "Неверный формат токена!"
GREETING = "Привет!"
REGISTRATION_DATA_LEFT = f'Ваши регистрационные данные остались прежними!\n{MENU_TEXT}'
NO_TOKEN_BUTTON = 'У меня нет токена'
CHOOSE_STATUS = 'Выберите ваш статус:\n'
CHOOSE_REGION = 'Введите код вашего региона:\n'
CHOOSE_CATEGORY = 'Выберите категорию соревнования\n'
CHOOSE_COMPETITION = 'Выберите ваше соревнование:\n'
VERIFY_PERSONAL_INFO = 'Подтвердите, что вы <b>{}</b>. Ваш регион - <b>{}</b>. '
VERIFY_COMPETITION = 'Ваше соревнование - <b>{}</b>.'

REGION_CODE_INPUT_STATE = -1
