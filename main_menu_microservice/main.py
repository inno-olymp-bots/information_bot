import logging

import telebot
from telebot import apihelper
from telebot import types as t

from core import settings as s
from core.markups import main_menu_markup
from core.texts import MENU_TEXT
from core.consumer import PickleConsumer


def callback(consumer, body):
    bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'menu', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='menu')

# Logger
logger = logging.getLogger(__name__)


@bot.message_handler(func=lambda message: True)
def menu(message: t.Message):
    bot.send_message(message.chat.id, MENU_TEXT, reply_markup=main_menu_markup())


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[MAIN MENU] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
