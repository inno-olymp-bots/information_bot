FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./main_menu_microservice/ /src/main_menu_microservice/
CMD [ "python", "./main_menu_microservice/main.py" ]
