import logging

import telebot
from telebot import apihelper

import core.database as db
import core.texts as tx
from core import callbacks as cb
from core import settings as s
from core.consumer import PickleConsumer
from participant_microservice import markups as mk
from participant_microservice.database import *
from participant_microservice.texts import *
from participant_microservice.utils import determine_day


def callback(consumer, body):
    if hasattr(body, 'data'):
        bot.process_new_callback_query([body])
    else:
        bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'participant', callback, s.RABBITMQ_USERNAME,
                     s.RABBITMQ_PASSWORD, exchange='info_bot', logger_name='participant')

# Logger
logger = logging.getLogger(__name__)


@bot.message_handler(regexp=tx.PARTICIPANT_EMOJI)
def choice_message(call):
    db.update_state(call.chat.id, 1)
    db.save_statistic(call.chat.id, c.PARTICIPANT)
    bot.send_message(chat_id=call.chat.id,
                     text=PARTICIPANT_QUESTION,
                     reply_markup=mk.participant_markup())


@bot.callback_query_handler(func=lambda call: call.data == cb.auth_decline_callback)
def choice_callback(call):
    bot.edit_message_text(chat_id=call.message.chat.id,
                          message_id=call.message.message_id,
                          text=PARTICIPANT_QUESTION,
                          reply_markup=mk.participant_markup())


@bot.callback_query_handler(func=lambda call: call.data == cb.back_participant_callback)
def back(call):
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=PARTICIPANT_QUESTION,
        reply_markup=mk.participant_markup()
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.catering_callback)
def catering(call):
    day = determine_day()
    tg_id = call.message.chat.id
    personal_catering = get_personal_meal(tg_id, day=day)

    bot.delete_message(tg_id, call.message.message_id)

    if personal_catering != "":
        bot.send_message(tg_id, personal_catering, parse_mode='Markdown')

    bot.send_message(
        chat_id=tg_id,
        text=CATERING_QUESTION,
        reply_markup=mk.catering_markup()
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.housing_callback)
def housing(call):
    tg_id = call.message.chat.id
    personal_housing = get_personal_housing(tg_id)

    bot.delete_message(tg_id, call.message.message_id)

    if personal_housing != "":
        bot.send_message(tg_id, personal_housing, parse_mode='Markdown')

    bot.send_message(
        chat_id=tg_id,
        text=HOUSING_QUESTION,
        reply_markup=mk.housing_markup()
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.transport_callback)
def transport(call):
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=TRANSPORT_QUESTION,
        reply_markup=mk.transport_markup()
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.attache_callback)
def attache(call):
    tg_id = call.message.chat.id
    attache_info = get_personal_attache(tg_id)

    bot.delete_message(tg_id, call.message.message_id)
    if attache_info != "":
        bot.send_message(tg_id, attache_info)
    bot.send_message(
        chat_id=tg_id,
        text=ATTACHE_MSG_TXT,
        reply_markup=mk.attache_markup(),
        parse_mode = 'Markdown'
    )


@bot.callback_query_handler(func=lambda call: call.data == cb.authorization_callback)
def authorization(call):
    user_id = db.get_id_by_tg_id(call.message.chat.id)
    auth_info = db.get_user_info(user_id)
    user_name = db.get_user_name(call.message.chat.id)
    text = tx.USER_INFO.format(user_name, tx.STATUS_CHOICES[str(auth_info['status'])])
    if auth_info['region'] is not None:
        text += tx.REGION_INFO.format(auth_info['region'])
    if auth_info['competition'] is not None:
        text += tx.COMPETITION_INFO.format(auth_info['competition'])
    text += AUTHORIZATION_TEXT
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=text,
        reply_markup=mk.accept_authorization_markup(),
        parse_mode='HTML'
    )


@bot.callback_query_handler(
    func=lambda call: call.data in [cb.meal_1_day_callback, cb.meal_2_day_callback, cb.meal_3_day_callback])
def catering_files(call):
    tg_id = call.message.chat.id
    bot.delete_message(tg_id, call.message.message_id)
    if call.data == cb.meal_1_day_callback:

        msg = get_meal_id(1, tg_id)
        if msg is not None:
            if is_meal_picture(1):
                bot.send_photo(tg_id, msg, caption=FIRST_DAY_MSG_TXT)
            else:
                bot.send_document(tg_id, msg, caption=FIRST_DAY_BTN_TXT)
        else:
            text = get_meal_text(1, tg_id)
            if text:
                bot.send_message(tg_id, text, parse_mode='Markdown')
            else:
                bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

    elif call.data == cb.meal_2_day_callback:

        msg = get_meal_id(2, tg_id)
        if msg is not None:
            if is_meal_picture(2):
                bot.send_photo(tg_id, msg, caption=SECOND_DAY_MSG_TXT)
            else:
                bot.send_document(tg_id, msg, caption=SECOND_DAY_BTN_TXT)
        else:
            text = get_meal_text(2, tg_id)
            if text:
                bot.send_message(tg_id, text, parse_mode='Markdown')
            else:
                bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

    else:

        msg = get_meal_id(3, tg_id)
        if msg is not None:
            if is_meal_picture(3):
                bot.send_photo(tg_id, msg, caption=THIRD_DAY_MSG_TXT)
            else:
                bot.send_document(tg_id, msg, caption=THIRD_DAY_BTN_TXT)
        else:
            text = get_meal_text(3, tg_id)
            if text:
                bot.send_message(tg_id, text, parse_mode='Markdown')
            else:
                bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, CATERING_QUESTION, reply_markup=mk.catering_markup(), parse_mode='Markdown')


@bot.callback_query_handler(func=lambda call: call.data in [cb.housing_place_callback, cb.housing_contact_callback])
def housing_info(call):
    tg_id = call.message.chat.id
    bot.delete_message(tg_id, call.message.message_id)
    if call.data == cb.housing_place_callback:

        msg = get_housing_id(tg_id)
        if msg is not None:
            if is_housing_picture():
                bot.send_photo(tg_id, msg, caption=HOUSING_PLACE_MSG_TXT)
            else:
                bot.send_document(tg_id, msg, caption=HOUSING_PLACE_MSG_TXT)
        else:
            text = get_housing_text(tg_id)
            if text:
                bot.send_message(tg_id, text, parse_mode='Markdown')
            else:
                bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

    else:

        msg = get_contacts(tg_id)
        if msg is not None and msg != "":
            bot.send_message(tg_id, msg)
        else:
            text = get_contacts(tg_id)
            if text:
                bot.send_message(tg_id, text, parse_mode='Markdown')
            else:
                bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, HOUSING_QUESTION, reply_markup=mk.housing_markup(), parse_mode='Markdown')


@bot.callback_query_handler(
    func=lambda call: call.data in [cb.transfer_callback, cb.shuttle_callback, cb.taxi_callback])
def transport_info(call):
    tg_id = call.message.chat.id
    day = determine_day()
    if call.data == cb.transfer_callback:
        msg, _ = get_personal_transport(tg_id, day=day)
    elif call.data == cb.shuttle_callback:
        _, msg = get_personal_transport(tg_id, day=day)
    else:
        msg = get_personal_taxi(tg_id)

    bot.delete_message(tg_id, call.message.message_id)
    if msg != "":
        bot.send_message(tg_id, msg, parse_mode='Markdown')
    else:
        bot.send_message(tg_id, tx.INFO_NOT_AVAILABLE)
    bot.send_message(tg_id, TRANSPORT_QUESTION, reply_markup=mk.transport_markup())


@bot.callback_query_handler(func=lambda call: call.data == cb.attache_list_callback)
def attache_info(call):
    tg_id = call.message.chat.id
    bot.delete_message(tg_id, call.message.message_id)
    msg = get_attache(tg_id)
    if msg is not None and msg != "":
        bot.send_message(tg_id, msg, parse_mode='Markdown')
    else:
        bot.send_message(call.message.chat.id, tx.INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, ATTACHE_MSG_TXT, reply_markup=mk.attache_markup(), parse_mode='Markdown')


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[PARTICIPANT] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
