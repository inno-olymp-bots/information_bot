FROM python:3.6-alpine
COPY requirements.txt .

RUN apk update && apk add build-base

RUN apk update && apk add --no-cache --update-cache postgresql-dev python3-dev musl-dev # for psycopg2
RUN apk add --no-cache openssl-dev libffi-dev # for cffi
RUN pip install --no-cache-dir --trusted-host pypi.python.org -r requirements.txt
