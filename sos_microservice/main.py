import logging

import telebot
from telebot import apihelper
from telebot import types as ty

from core.texts import SOS_EMOJI
from core.consumer import PickleConsumer
from core import settings as s
from core.database import update_state

from sos_microservice.database import *
from sos_microservice.texts import *


def callback(consumer, body):
    bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'SOS', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='SOS')

# Logger
logger = logging.getLogger(__name__)


@bot.message_handler(regexp=SOS_EMOJI)
def sos(message: ty.Message):
    bot.send_message(message.chat.id, ASK_TO_WRITE_QUESTION)
    update_state(message.chat.id, 2)


@bot.message_handler(content_types=["text"])
def response(message):
    msg = get_message_to_group(message)
    groups_id = get_groups_id()
    for group_id in groups_id:
        if group_id is not None and group_id[0] != "":
            bot.send_message(group_id[0], msg)

    save_msg(message)
    update_state(message.chat.id, 1)
    bot.send_message(message.chat.id, RESPONSE)

    # Вычисление жулика
    if s.CHEATERS_FINDER_FLAG:
        text = message.text
        text = text.upper()
        bad_words = ['SELECT', 'DROP', 'CREATE', 'UPDATE', 'WHERE']
        cheater = False
        for word in bad_words:
            if word in text:
                cheater = True
        if cheater:
            bot.send_sticker(message.chat.id, 'CAADAgADtwYAAmMr4glix_Q1e5JDQAI')
            bot.send_message(message.chat.id, 'SQL инъекции детям не игрушка')


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[SOS] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
