import logging

import telebot
from telebot import apihelper

from broadcast_microservice.database import *
from core.consumer import PickleConsumer


def callback(consumer, body):
    try:
        sender(body)
        consumer.logger.info('Consumed')
    except Exception as e:
        logging.error(e, exc_info=True)
        consumer.logger.error('Message not delivered.')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'broadcast', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='broadcast')

# Logger
logger = telebot.logger
telebot.logger.setLevel(s.LOG_LEVEL)


def carefully_send(tg_id, text):
    try:
        bot.send_message(tg_id, text, parse_mode='Markdown')
    except telebot.apihelper.ApiException:
        logger.info(f'Cannot send message to {tg_id}.')


def sender(body):
    state = body[0]
    msg_id = body[1]

    if state == 0:  # send to all
        text = get_text_by_msg_id(msg_id)
        users = get_all_users()

        for user in users:
            tg_id = user[0]
            carefully_send(tg_id, text)

        unregistered_users = get_unregistered_users()
        for user in unregistered_users:
            tg_id = user[0]
            carefully_send(tg_id, text)

        set_sent_all_message(msg_id)

    elif state == 1:  # send to person
        user_id, text = get_tg_id_and_text_by_msg_id(msg_id)
        tg_id = get_user_tg_id(user_id)
        if tg_id is not None:
            carefully_send(tg_id, text)
        set_sent_personal_message(msg_id)

    elif state == 2:  # send to teams
        team_ids, text = get_team_ids_and_text_by_msg_id(msg_id)
        for team_id in team_ids:
            teammates = get_teammates(team_id)
            for user in teammates:
                user_id = user[0]
                tg_id = get_user_tg_id(user_id)
                if tg_id is not None:
                    carefully_send(tg_id, text)
        set_sent_team_message(msg_id)

    elif state == 3:  # send by competitions
        competition_ids, text = get_competition_ids_and_text_by_msg_id(msg_id)
        for competition_id in competition_ids:
            teams = get_teams_by_competition(competition_id)
            for team in teams:
                team_id = team[0]
                teammates = get_teammates(team_id)
                for user in teammates:
                    user_id = user[0]
                    tg_id = get_user_tg_id(user_id)
                    if tg_id is not None:
                        carefully_send(tg_id, text)
        set_sent_competition_message(msg_id)

    elif state == 4:  # send by regions
        region_ids, text = get_region_ids_and_text_by_msg_id(msg_id)
        for region_id in region_ids:
            people = get_users_of_region(region_id)
            for user in people:
                tg_id = user[0]
                carefully_send(tg_id, text)
        set_sent_region_message(msg_id)

    elif state == 5:  # send by status
        status, text = get_status_and_text_by_msg_id(msg_id)
        people = get_users_by_status(status)
        for user in people:
            tg_id = user[0]
            carefully_send(tg_id, text)
        set_sent_status_message(msg_id)


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[BROADCAST] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
