FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./broadcast_microservice/ /src/broadcast_microservice/
CMD [ "python", "./broadcast_microservice/main.py" ]
