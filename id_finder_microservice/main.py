import logging
from time import sleep

import telebot
from telebot import apihelper

from core import settings as s
from core.consumer import PickleConsumer
from id_finder_microservice.database import set_file_id, get_receiver_id, is_picture


def callback(consumer, body):
    sender(body)
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'id_finder', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='id_finder')

# Logger
logger = logging.getLogger(__name__)


def sender(body):
    sleep(5)
    tg_id = get_receiver_id()

    for file_and_id in body:
        file = file_and_id[0]
        feature_file_id = file_and_id[1]

        if file is not None:

            if is_picture(feature_file_id):
                message = bot.send_photo(tg_id, file)
                file_tg_id = message.__getattribute__('json').get('photo')[0].get('file_id')
            else:
                message = bot.send_document(tg_id, file)
                file_tg_id = message.__getattribute__('json').get('document').get('file_id')

            logger.log(logging.INFO, f'[ID FINDER] {feature_file_id}')
            set_file_id(feature_file_id, file_tg_id)


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[ID FINDER] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
