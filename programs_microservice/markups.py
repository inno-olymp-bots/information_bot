from telebot import types
from core.callbacks import olymp_schedule_callback, activity_callback, master_schedule_callback
from programs_microservice import texts as t


def program_markup():
    keyboard = types.InlineKeyboardMarkup()
    olymp_button = types.InlineKeyboardButton(text=t.OLYMP_BTN_TXT, callback_data=olymp_schedule_callback)
    master_button = types.InlineKeyboardButton(text=t.MASTER_CLASS_BTN_TXT, callback_data=master_schedule_callback)
    activity_button = types.InlineKeyboardButton(text=t.ACTIVITY_BTN_TXT, callback_data=activity_callback)

    keyboard.add(olymp_button, master_button)
    keyboard.add(activity_button)
    return keyboard
