import psycopg2
from core import settings as s
from core import configs as c


def get_data(feature, tg_id=1, action=-1):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT {feature} FROM files_pdffields')
    data = cursor.fetchall()
    if action != -1:
        cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
        user = cursor.fetchall()[0]
        status, token = user[0], user[1]
        has_token = True if (token != "") and (token is not None) else False
        cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                       f' ({tg_id}, {action}, now(), {status}, {has_token})')
        connection.commit()
    connection.close()
    try:
        return data[0][0]
    except:
        return None


def get_olymp_id(tg_id):
    return get_data('olymp_schedule_id', tg_id, c.PROGRAM_OLYMP)


def get_master_id(tg_id):
    return get_data('master_schedule_id', tg_id, c.PROGRAM_MASTER)


def get_activity_id(tg_id):
    return get_data('activity_schedule_id', tg_id, c.PROGRAM_ACTIVITY)


def get_olymp_text(tg_id):
    return get_data('olymp_schedule_text', tg_id, c.PROGRAM_OLYMP)


def get_master_text(tg_id):
    return get_data('master_schedule_text', tg_id, c.PROGRAM_MASTER)


def get_activity_text(tg_id):
    return get_data('activity_schedule_text', tg_id, c.PROGRAM_ACTIVITY)


def is_olymp_picture():
    return get_data('olymp_schedule_is_pct')


def is_master_picture():
    return get_data('master_schedule_is_pct')


def is_activity_picture():
    return get_data('activity_schedule_is_pct')
