import logging

import telebot
from telebot import apihelper
from telebot import types as t

from core.callbacks import olymp_schedule_callback, master_schedule_callback, activity_callback
from core.consumer import PickleConsumer
from core.database import update_state, save_statistic
from core.texts import PROGRAM_EMOJI, INFO_NOT_AVAILABLE
from programs_microservice.database import *
from programs_microservice.markups import program_markup
from programs_microservice.texts import *


def callback(consumer, body):
    if hasattr(body, 'data'):
        bot.process_new_callback_query([body])
    else:
        bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'programs', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='programs')

# Logger
logger = logging.getLogger(__name__)


@bot.message_handler(regexp=PROGRAM_EMOJI)
def choice(message: t.Message):
    save_statistic(message.chat.id, c.PROGRAM)
    update_state(message.chat.id, 1)
    bot.send_message(message.chat.id, PROGRAM_SCHEDULE_QUESTION, reply_markup=program_markup())


@bot.callback_query_handler(func=lambda c: c.data == olymp_schedule_callback)
def olymp(message: t.CallbackQuery):
    tg_id = message.message.chat.id
    msg = get_olymp_id(tg_id)
    bot.delete_message(tg_id, message.message.message_id)
    if msg is not None:

        if is_olymp_picture():
            bot.send_photo(tg_id, msg, caption=OLYMP_MSG_TXT)
        else:
            bot.send_document(tg_id, msg, caption=OLYMP_MSG_TXT)

    else:
        text = get_olymp_text(tg_id)
        if text:
            bot.send_message(tg_id, text, parse_mode='Markdown')
        else:
            bot.send_message(tg_id, INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, PROGRAM_SCHEDULE_QUESTION, reply_markup=program_markup())


@bot.callback_query_handler(func=lambda c: c.data == master_schedule_callback)
def master(message: t.CallbackQuery):
    tg_id = message.message.chat.id
    msg = get_master_id(tg_id)
    bot.delete_message(tg_id, message.message.message_id)
    if msg is not None:

        if is_master_picture():
            bot.send_photo(tg_id, msg, caption=MASTER_CLASS_MSG_TXT)
        else:
            bot.send_document(tg_id, msg, caption=MASTER_CLASS_MSG_TXT)

    else:
        text = get_master_text(tg_id)
        if text:
            bot.send_message(tg_id, text, parse_mode='Markdown')
        else:
            bot.send_message(tg_id, INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, PROGRAM_SCHEDULE_QUESTION, reply_markup=program_markup())


@bot.callback_query_handler(func=lambda c: c.data == activity_callback)
def activity(message: t.CallbackQuery):
    tg_id = message.message.chat.id
    msg = get_activity_id(tg_id)
    bot.delete_message(tg_id, message.message.message_id)
    if msg is not None:

        if is_activity_picture():
            bot.send_photo(tg_id, msg, caption=ACTIVITY_MSG_TXT)
        else:
            bot.send_document(tg_id, msg, caption=ACTIVITY_MSG_TXT)

    else:
        text = get_activity_text(tg_id)
        if text:
            bot.send_message(tg_id, text, parse_mode='Markdown')
        else:
            bot.send_message(tg_id, INFO_NOT_AVAILABLE)

    bot.send_message(tg_id, PROGRAM_SCHEDULE_QUESTION, reply_markup=program_markup())


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[PROGRAMS] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
