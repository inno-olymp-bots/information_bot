FROM innoolympbots/info_service

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./programs_microservice/ /src/programs_microservice/
CMD [ "python", "./programs_microservice/main.py" ]
