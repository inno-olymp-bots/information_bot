import logging

import telebot
from telebot import apihelper

from core import database
from core.settings import *
from core.publisher import PicklePublisher
from core.texts import *
from core.database import is_key_valid, register_chat


def listener(messages):
    for m in messages:
        if m.content_type == 'text':
            print(str(m.chat.first_name) + " [" + str(m.chat.id) + "]: " + m.text)


# Set proxy
apihelper.proxy = PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(TOKEN, num_threads=10)
bot.set_update_listener(listener)

# RabbitMQ
queues = ['registration', 'table', 'SOS', 'menu', 'navigation', 'programs', 'participant', 'viewer']
pub = PicklePublisher(RABBITMQ_HOST, RABBITMQ_PORT, queues, RABBITMQ_USERNAME, RABBITMQ_PASSWORD, exchange='info_bot',
                      logger_name='polling')

# Logger
logger = logging.getLogger(__name__)


@bot.callback_query_handler(
    func=lambda call: call.data.startswith('reg=') or (database.get_state(call.message.chat.id) <= 0))
@bot.message_handler(func=lambda m: (m.chat.type == "private") and (database.get_state(m.chat.id) <= 0))
def registration(message):
    logger.log(logging.INFO, 'Queue: ' + 'registration')
    pub.publish_message(
        message,
        'registration'
    )


@bot.message_handler(regexp=TABLE_EMOJI)
@bot.callback_query_handler(func=lambda c: c.data.startswith('tab='))
def table(message):
    logger.log(logging.INFO, 'Queue: ' + 'table')

    pub.publish_message(
        message,
        'table'
    )


@bot.message_handler(regexp=NAVIGATION_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('navi='))
def navigation(message):
    logger.log(logging.INFO, 'Queue: ' + 'navigation')
    pub.publish_message(
        message,
        'navigation'
    )


@bot.message_handler(regexp=PROGRAM_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('prog='))
def programs_schedule(message):
    logger.log(logging.INFO, 'Queue: ' + 'programs')
    pub.publish_message(
        message,
        'programs'
    )


@bot.message_handler(regexp=PARTICIPANT_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('part='))
def programs_schedule(message):
    logger.log(logging.INFO, 'Queue: ' + 'participant')
    pub.publish_message(
        message,
        'participant'
    )


@bot.message_handler(regexp=VIEWER_EMOJI)
@bot.callback_query_handler(func=lambda m: m.data.startswith('view='))
def viewer(message):
    logger.log(logging.INFO, 'Queue: ' + 'viewer')
    pub.publish_message(
        message,
        'viewer'
    )


@bot.message_handler(
    func=lambda m: (m.chat.type == "private") and (database.get_state(m.chat.id) == 2) or (SOS_EMOJI == m.text))
def sos(message):
    logger.log(logging.INFO, 'Queue: ' + 'SOS')
    pub.publish_message(
        message,
        'SOS'
    )


@bot.message_handler(func=lambda m: m.chat.type in ["group", "supergroup", ])
def group(message):
    logger.log(logging.INFO, 'Queue: ' + 'group')
    msg = message.text.split(" ")
    if msg[0] == '/key':
        if is_key_valid(message.text):
            key = msg[1]
            register_chat(message.chat.id, key)
            bot.send_message(message.chat.id, GROUP_REGISTERED)
        else:
            bot.send_message(message.chat.id, GROUP_KEY_INVALID)


@bot.message_handler(func=lambda m: m.chat.type == "private")
def main_menu(message):
    logger.log(logging.INFO, 'Queue: ' + 'menu')
    pub.publish_message(
        message,
        'menu'
    )


if __name__ == '__main__':

    try:
        pub.run()
        logger.log(logging.INFO, '[POLLING] Waiting for messages. To exit press CTRL+C')
        bot.remove_webhook()
        bot.polling(none_stop=True)
        raise KeyboardInterrupt

    except KeyboardInterrupt:
        pub.terminate()
