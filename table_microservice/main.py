import logging
import table_microservice.texts as t
from datetime import datetime
from datetime import timedelta

import telebot
from telebot import apihelper

from core import settings as s
from core.database import update_state, save_statistic

from table_microservice import database
from table_microservice import render
from table_microservice import sheetparser
import table_microservice.markups as m
import core.callbacks as cb
from core.consumer import PickleConsumer
from core import configs as c
import pytz


def callback(consumer, body):
    if hasattr(body, 'data'):
        bot.process_new_callback_query([body])
    else:
        bot.process_new_messages([body])
    consumer.logger.info('Consumed')


# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# RabbitMQ
con = PickleConsumer(s.RABBITMQ_HOST, s.RABBITMQ_PORT, 'table', callback, s.RABBITMQ_USERNAME, s.RABBITMQ_PASSWORD,
                     exchange='info_bot', logger_name='table')

# Logger
logger = logging.getLogger(__name__)


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.category_callback))
def categories(call):
    data = call.data.split('=')
    cat_comp = database.get_category_name(data[2])
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=(t.CATEGORY % cat_comp) + t.CHOOSE_COMPETITION,
        reply_markup=m.inline_competitions_markup(data[2]),
        parse_mode='HTML'
    )


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.tab_competition_callback))
def competitions(call):
    data = call.data.split('=')
    comp, cat_comp = database.get_competition_name_and_category(data[2])
    text = (t.CATEGORY % cat_comp) + (t.COMPETITION % comp) + t.CHOOSE_SECTION
    markup = m.inline_sections_markup(data[2])
    if len(markup.to_dic()['inline_keyboard']) == 1:
        text = t.NO_SECTIONS_AVAILABLE
    bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=text,
        reply_markup=markup,
        parse_mode='HTML'
    )


@bot.callback_query_handler(func=lambda call: call.data.startswith(cb.section_callback))
def sections(call):
    data = call.data.split('=')
    comp, cat_comp = database.get_competition_name_and_category(data[3])
    database.save_statistic(call.message.chat.id, cat_comp + " - " + comp)
    sect, _ = database.get_sections_and_category(data[3])
    if sect[int(data[2])]['active']:
        sect, parser = sect[int(data[2])]['name'], sect[int(data[2])]['parser']
        photo_type = {
            'sect': int(data[2]),
            'comp': int(data[3]),
        }
        send_table(photo_type=photo_type,
                   chat_id=call.message.chat.id,
                   caption=(t.CATEGORY % cat_comp) + (t.COMPETITION % comp) + (t.SECTION % sect),
                   parser_id=parser,
                   competition_id=data[3],
                   del_message_id=call.message.message_id)
    else:
        bot.delete_message(call.message.chat.id, call.message.message_id)
    markup = m.inline_sections_markup(data[3])
    if len(markup.to_dic()['inline_keyboard']) == 1:
        text = t.NO_SECTIONS_AVAILABLE
    else:
        text = call.message.text
    bot.send_message(call.message.chat.id, text, reply_markup=markup, parse_mode='HTML')


def send_table(photo_type, chat_id, caption, parser_id, competition_id, del_message_id=None, tg_id=None):
    # get last update time and photo_id of the rating table
    update_time, photo_id = database.get_time_photo(photo_type)
    logger.critical(f"Photo_type is {photo_type}")
    comp, cat_comp = database.get_competition_name_and_category(photo_type['comp'])
    logger.critical(f"Competition name and category: {comp}, {cat_comp}")

    # if the last update was more than one minute ago update an image of table and send it
    if update_time is None or datetime.now(pytz.timezone(s.TIME_ZONE)) - timedelta(minutes=1) >= update_time: # WHYYY 300??
        df = sheetparser.query(parser_id, competition_id, tg_id)
        if df is not None:
            image = render.image(df)
            if del_message_id:
                bot.delete_message(chat_id, del_message_id)
            message = bot.send_photo(chat_id=chat_id,
                                     photo=image,
                                     caption=caption,
                                     parse_mode='HTML')
            photo_id = message.__getattribute__('json').get('photo')[1].get('file_id')
            database.save_photo(photo_type, photo_id, datetime.now(pytz.timezone(s.TIME_ZONE)))
    # otherwise send photo_id
    else:
        if del_message_id:
            bot.delete_message(chat_id, del_message_id)
        bot.send_photo(chat_id=chat_id,
                       photo=photo_id,
                       caption=caption,
                       parse_mode='HTML')


def my_competitions(message):
    competition_ids, tg_id = database.get_competitions_by_id(message.chat.id)
    for competition_id in competition_ids:
        comp, cat_comp = database.get_competition_name_and_category(competition_id)
        section_list, _ = database.get_sections_and_category(competition_id)
        sect_index = -1
        if section_list[1]['active']:
            sect_index = 1
        elif section_list[0]['active']:
            sect_index = 0
        if sect_index != -1:
            sect, parser = section_list[sect_index]['name'], section_list[sect_index]['parser']
            photo_type = {
                'sect': sect_index,
                'comp': competition_id
            }
            if tg_id:
                photo_type['tg_id'] = tg_id
            send_table(photo_type=photo_type,
                       chat_id=message.chat.id,
                       caption=t.MY_RESULTS + (t.CATEGORY % cat_comp) + (t.COMPETITION % comp) + (t.SECTION % sect),
                       parser_id=parser,
                       competition_id=competition_id,
                       tg_id=tg_id)


@bot.message_handler(func=lambda message: True)
@bot.callback_query_handler(func=lambda call: call.data == cb.back_categories_callback)
def results(obj):
    if hasattr(obj, 'data'): # that means that obj is callbackquery
        update_state(obj.message.chat.id, 1)
        bot.edit_message_text(
            chat_id=obj.message.chat.id,
            message_id=obj.message.message_id,
            text=t.CHOOSE_CATEGORY,
            reply_markup=m.inline_categories_markup(),
            parse_mode='HTML'
        )
    else:
        update_state(obj.chat.id, 1) 
        my_competitions(obj)
        save_statistic(obj.chat.id, c.RESULTS)
        bot.send_message(
            chat_id=obj.chat.id,
            text=t.CHOOSE_CATEGORY,
            reply_markup=m.inline_categories_markup(),
            parse_mode='HTML'
        )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[TABLE] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
