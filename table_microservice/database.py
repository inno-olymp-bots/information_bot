import psycopg2
from core import configs as c
import core.settings as s


def get_time_photo(photo_type):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if 'tg_id' not in photo_type:
        cursor.execute(
            'SELECT update_time, photo_id FROM table_screenshots WHERE section = %s AND competition_id = %s AND tg_id IS NULL',
            (photo_type['sect'], photo_type['comp']))
    else:
        cursor.execute(
            'SELECT update_time, photo_id FROM table_screenshots WHERE section = %s AND competition_id = %s AND tg_id = %s',
            (photo_type['sect'], photo_type['comp'], photo_type['tg_id']))
    time = cursor.fetchall()
    connection.close()
    if len(time) > 0:
        return time[0][0], time[0][1]
    else:
        return None, None


def save_photo(photo_type, photo_id, time):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    if 'tg_id' not in photo_type:
        cursor.execute(
            'SELECT update_time, photo_id FROM table_screenshots WHERE section = %s AND competition_id = %s AND tg_id IS NULL',
            (photo_type['sect'], photo_type['comp']))
        if len(cursor.fetchall()) == 0:
            cursor.execute(
                'INSERT INTO table_screenshots (photo_id, update_time, section, competition_id, tg_id) VALUES (%s, %s, %s, %s, NULL)',
                (photo_id, time, photo_type['sect'], photo_type['comp']))
        else:
            cursor.execute(
                'UPDATE table_screenshots SET photo_id = %s, update_time = %s WHERE section = %s AND competition_id = %s AND tg_id IS NULL',
                (photo_id, time, photo_type['sect'], photo_type['comp']))
    else:
        cursor.execute(
            'SELECT update_time, photo_id FROM table_screenshots WHERE section = %s AND competition_id = %s AND tg_id = %s',
            (photo_type['sect'], photo_type['comp'], photo_type['tg_id']))
        if len(cursor.fetchall()) == 0:
            cursor.execute(
                'INSERT INTO table_screenshots (photo_id, update_time, section, competition_id, tg_id) VALUES (%s, %s, %s, %s, %s)',
                (photo_id, time, photo_type['sect'], photo_type['comp'], photo_type['tg_id']))
        else:
            cursor.execute(
                'UPDATE table_screenshots SET photo_id = %s, update_time = %s WHERE section = %s AND competition_id = %s AND tg_id = %s',
                (photo_id, time, photo_type['sect'], photo_type['comp'], photo_type['tg_id']))
    connection.commit()
    connection.close()


def get_categories():
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id, name FROM competition_categories')
    results = cursor.fetchall()
    connection.commit()
    connection.close()
    return results


def get_category_name(id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT name FROM competition_categories WHERE id = {}'.format(id))
    result = cursor.fetchone()
    connection.commit()
    connection.close()
    return result[0]


def get_competition(index):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id, name FROM competitions WHERE category_id = {}'.format(index))
    results = cursor.fetchall()
    connection.commit()
    connection.close()
    return results


def get_competition_name_and_category(id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(
        'SELECT comp.name, cat.name FROM competitions AS comp JOIN competition_categories AS cat ON comp.category_id = cat.id WHERE comp.id = {}'.format(
            id))
    result = cursor.fetchone()
    connection.commit()
    connection.close()
    return result[0], result[1]


def get_competition_link(id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT link FROM competitions WHERE id = {}'.format(id))
    results = cursor.fetchone()
    connection.commit()
    connection.close()
    return results[0]


def get_sections_and_category(index):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(
        'SELECT fst_round_active, fst_round_name, fst_round_parser_id, snd_round_active, snd_round_name, snd_round_parser_id, winners_active, winners_name, winners_parser_id, category_id FROM competitions WHERE id = {}'.format(
            index))
    node = cursor.fetchone()
    result = []
    for i in range(0, 9, 3):
        result.append({'active': node[i], 'name': node[i + 1], 'parser': node[i + 2]})
    connection.commit()
    connection.close()
    return result, node[9]


def get_competitions_by_id(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id, token FROM users WHERE tg_id = {}'.format(tg_id))
    row = cursor.fetchone()
    user_id, token = row[0], row[1]
    if token:
        cursor.execute(
            'SELECT t.competition_id FROM users_team AS t JOIN users_team_participants AS part ON t.id = part.team_id WHERE part.user_id = {}'.format(
                user_id))
    else:
        cursor.execute('SELECT competition_id FROM users WHERE id = {}'.format(user_id))
    result = [i[0] for i in cursor.fetchall()]
    if result is None or result == [None]:
        result = []
    connection.commit()
    connection.close()
    if token:
        return result, tg_id
    else:
        return result, None


def get_team_names_by_id(tg_id, competition_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id FROM users WHERE tg_id = {}'.format(tg_id))
    user_id = cursor.fetchone()[0]
    cursor.execute(
        'SELECT t.name FROM users_team AS t JOIN users_team_participants AS part ON t.id = part.team_id WHERE part.user_id = {} AND t.competition_id = {}'.format(
            user_id, competition_id))
    result = [str(i[0]) for i in cursor.fetchall()]
    connection.commit()
    connection.close()
    return result


def get_parser_by_id(parser_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT list_name, range, columns FROM parsers WHERE id = %s', (parser_id,))
    row = cursor.fetchone()
    result = {
        'list_name': row[0],
        'range': row[1],
        'columns': [str(i).replace('\\n', '\n') for i in row[2]]
    }
    connection.commit()
    connection.close()
    return result


def save_statistic(tg_id, info):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()

    cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
    user = cursor.fetchall()[0]
    status, token = user[0], user[1]
    has_token = True if (token != "") and (token is not None) else False
    cursor.execute(
        f"INSERT INTO users_statistic (tg_id, query, datetime, information, has_token, status) VALUES"
        f" ({tg_id}, {c.RESULTS}, now(), '{info}', {has_token}, {status})")
    connection.commit()
    connection.close()
