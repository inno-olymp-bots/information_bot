from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from table_microservice import database
import core.settings as s
import pandas
import os
import logging

service = None
logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)


def get_service():
    # get service for making requests
    global service

    if service is None:
        # Setup the Sheets API
        folder = os.path.dirname(os.path.abspath(__file__)) + '/'
        store = file.Storage(folder + 'credentials.json')
        creds = store.get()

        if not creds or creds.invalid:
            logger.critical(msg="There is no credentials.json file! Run the generate_creds.sh script to get it")
            raise RuntimeError("No credentials were given")

        service = build('sheets', 'v4', http=creds.authorize(Http()), cache_discovery=False)
        
    return service


def query(parser_id, competition_id, tg_id=None):
    spreadsheet_id = database.get_competition_link(competition_id).split('/d/')[1].split('/')[0]
    team_names = []
    if tg_id is not None:
        team_names = database.get_team_names_by_id(tg_id, competition_id)
    parser = database.get_parser_by_id(parser_id)
    range = parser['list_name'] + '!' + parser['range']
    # make query to Google Sheets API
    result = get_service().spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                       range=range).execute()
    values = []

    for val in result.get('values', []):
        if val[0] != "":
            if (tg_id is not None) and (val[0] in team_names): # this is to make the user see only the team it belongs to
                values.append(val)
            elif tg_id is None:
                values.append(val)
        else:
            break

    # create DataFrame object using values from a sheet
    if len(values) > 0:
        width = len(values[0])
        df = pandas.DataFrame(values, columns=parser['columns'][:width])
        return df
    else:
        logger.debug("Len of values <= 0")
        return None
