FROM python:3.6
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY table_microservice/table_requirements.txt table_requirements.txt
RUN pip install --no-cache-dir -r table_requirements.txt

RUN mkdir /src/
WORKDIR /src/

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./table_microservice/ /src/table_microservice/
CMD [ "python", "./table_microservice/main.py" ]
