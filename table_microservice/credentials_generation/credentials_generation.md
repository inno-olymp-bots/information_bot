Before starting the table microservice, generate the credentials.json file.

### How to generate the credentials:
0. This instruction assumes that you have the python3 and its pip module installed and available via console. 
   Run `python3 -V && python3 -m pip -V` to test if they're installed correctly
1. Get your client_secret.json from google and save it in the parent directory of credentials_generation folder. Go to google api console to obtain it. 
2. Run cd into the directory with the [generate_creds.sh](generate_creds.sh) file.
   If you are in the root of the project, the command will be the following:      
    ```
    cd table_microservice/credentials_generation
    ```
3. Run the script itself
   ```
   bash generate_creds.sh
   ```
4. The file credentials.json is created in both [table_microservice](table_microservice) and [credentials_generation](table_microservice/credentials_generation) directories
5. Now you can start the table microservice and it will work as supposed