from oauth2client import file, client, tools
from oauth2client.tools import argparser
import os

SHEETS_API_SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'

def generate():
    folder = os.path.dirname(os.path.abspath(__file__)) + '/'
    store = file.Storage(folder + 'credentials.json')

    flow = client.flow_from_clientsecrets(folder + 'client_secret.json', SHEETS_API_SCOPES, prompt='consent')
    args = argparser.parse_args()
    args.noauth_local_webserver = True
    creds = tools.run_flow(flow, store, args)

if __name__ == '__main__':
    generate()