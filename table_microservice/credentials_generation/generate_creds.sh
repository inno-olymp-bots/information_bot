#!/usr/bin/env bash
echo "RUN THIS SCRIPT ONLY FROM ITS DIRECTORY"
wait 2
cp ../client_secret.json .
python3 -m pip install virtualenv
python3 -m virtualenv venv
source venv/bin/activate
pip install -r requirements_gen.txt
python get_credentials.py
cp credentials.json ..
deactivate
rm -rf venv
echo "credentials.json is generated"