import table_microservice.texts as t
import telebot
from table_microservice import database
import core.callbacks as cb
from core.texts import BACK


def inline_categories_markup():
    keyboard = telebot.types.InlineKeyboardMarkup()
    # format of callback data = 'cat_comp=CompetitionCategory_id
    for cat in database.get_categories():
        keyboard.add(
            telebot.types.InlineKeyboardButton(text=cat[1], callback_data=cb.category_callback + f'{cat[0]}'))
    return keyboard


def inline_competitions_markup(index):
    keyboard = telebot.types.InlineKeyboardMarkup()
    # format of callback data = 'comp=Competition_id
    for cat in database.get_competition(index):
        keyboard.add(
            telebot.types.InlineKeyboardButton(text=cat[1], callback_data=cb.tab_competition_callback + f'{cat[0]}'))
    # BACK button
    keyboard.add(telebot.types.InlineKeyboardButton(text=BACK, callback_data=cb.back_categories_callback))
    return keyboard


def inline_sections_markup(index):
    keyboard = telebot.types.InlineKeyboardMarkup()
    res, category_id = database.get_sections_and_category(index)
    # format of callback data = 'sect=Section_index=Competition_id'
    for i in range(len(res)):
        if res[i]['active']:
            keyboard.add(telebot.types.InlineKeyboardButton(text=res[i]['name'],
                                                            callback_data=cb.section_callback + f'{i}={index}'))
    # BACK button
    keyboard.add(
        telebot.types.InlineKeyboardButton(text=BACK, callback_data=cb.category_callback + f'{category_id}'))
    return keyboard
