city_callback = "navi=city"
area_callback = "navi=area"


olymp_schedule_callback = "prog=olymp"
master_schedule_callback = "prog=master"
activity_callback = "prog=activity"


catering_callback = "part=catering"
housing_callback = "part=housing"
transport_callback = "part=transport"
attache_callback = "part=attache"
back_participant_callback = "part=back"

meal_1_day_callback = "part=first"
meal_2_day_callback = "part=second"
meal_3_day_callback = "part=third"

housing_place_callback = "part=place"
housing_contact_callback = "part=contact"

transfer_callback = "part=transfer"
shuttle_callback = "part=shuttle"
taxi_callback = "part=taxi"

attache_list_callback = "part=list"

authorization_callback = "part=auth"
auth_decline_callback = "part=decline"


auth_accept_callback = "reg=accept"

no_token_callback = "reg=no_token"
status_callback = "reg=st="
reg_competition_callback = "reg=comp="
reg_category_callback = "reg=cat_comp="
handle_answer_callback = "reg=ans="

confirm_callback = "reg=confirm"


back_viewer_callback = "view=back"
viewer_transport_callback = "view=transport"
viewer_auth_callback = "view=auth"

viewer_shuttle_callback = "view=shuttle"
viewer_taxi_callback = "view=taxi"
viewer_shuttle_1_callback = "view=first"
viewer_shuttle_2_callback = "view=second"
viewer_shuttle_3_callback = "view=third"

back_categories_callback = "tab=b_cat"
section_callback = "tab=sect="
tab_competition_callback = "tab=comp="
category_callback = "tab=cat_comp="
