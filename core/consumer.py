# -*- coding: utf-8 -*-

import functools
import pickle
from typing import Callable

from core.exchange import MessageQueueOperator


class Consumer(MessageQueueOperator):
    """
    This is a consumer that will handle unexpected interactions
    with RabbitMQ such as channel and connection closures.

    If RabbitMQ closes the connection, it will reopen it. You should
    look at the output, as there are limited reasons why the connection may
    be closed, which usually are tied to permission related issues or
    socket timeouts.

    If the channel is closed, it will indicate a problem with one of the
    commands that were issued and that should surface in the output as well.

    """

    def __init__(self,
                 host: str,
                 port: int,
                 queue_name: str,
                 consume: Callable,
                 username: str = 'guest',
                 password: str = 'guest',
                 exchange: str = 'some_exchange',
                 logger_name: str = 'ConsumerMQ'):
        """
        Create a new instance of the consumer class.
        :param host:
        :param port:
        :param queue_name:
        :param username:
        :param password:
        :param exchange:
        """
        super().__init__(host, port, username, password, [queue_name], exchange, logger_name)

        self.queue_name = queue_name
        self._consumer_tag = None
        self._consume = consume

    def start(self):
        self.logger.info('Issuing consumer related RPC commands')
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(self.on_message,
                                                         queue=self.queue_name)

    def add_on_cancel_callback(self):
        self.logger.info('Adding consumer cancellation callback')
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        self.logger.info('Consumer was cancelled remotely, shutting down: %r', method_frame)
        if self._channel:
            self._channel.close()

    def on_message(self, channel, basic_deliver, properties, body):
        self.logger.info('Received message # %s from %s: %s',
                             basic_deliver.delivery_tag, properties.app_id, body)
        self.acknowledge_message(basic_deliver.delivery_tag)

        self._consume(self, body)

    def acknowledge_message(self, delivery_tag: int):
        self.logger.info('Acknowledging message %s', delivery_tag)
        self._channel.basic_ack(delivery_tag)

    def stop_consuming(self):
        if self._channel:
            self.logger.info('Sending a Basic.Cancel RPC command to RabbitMQ')
            cb = functools.partial(self.on_cancelok,
                                   userdata=self._consumer_tag)
            self._channel.basic_cancel(consumer_tag=self._consumer_tag,
                                       callback=cb)

    def on_cancelok(self, frame, userdata: str):
        self.logger.info('RabbitMQ acknowledged the cancellation of the consumer: %s', userdata)
        self.close_channel()


class PickleConsumer(Consumer):
    def on_message(self, channel, basic_deliver, properties, body):
        super().on_message(channel,
                           basic_deliver,
                           properties,
                           pickle.loads(body, fix_imports=False))


def consume(body):
    print('Consumed', body)


if __name__ == '__main__':
    example = PickleConsumer('localhost', 5672, 'test', consume, exchange='info_bot')
    try:
        example.run()
    except KeyboardInterrupt:
        example.stop()
