import functools
import logging
import pika


class MessageQueueOperator:
    """
    Connection operator that will handle unexpected interactions with RabbitMQ.
    """

    def __init__(self,
                 host: str,
                 port: int,
                 username: str,
                 password: str,
                 queues: list,
                 exchange: str,
                 logger_name: str,
                 log_level: str = logging.ERROR):
        """
        Create a new instance of the consumer class.
        :param host:
        :param port:
        :param username:
        :param password:
        :param exchange:
        """

        self._url = f'amqp://{username}:{password}@{host}:{port}/%2F'
        self.host = host

        self.exchange = exchange
        self.queues = queues

        self._connection: pika.SelectConnection = None
        self._channel = None
        self._closing = False

        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(log_level)

        if self.exchange == '':
            raise ValueError('Default exchange not permitted.')

    def connect(self):
        self.logger.info(f'Connecting to the {self.host}')

        return pika.SelectConnection(parameters=pika.URLParameters(self._url),
                                     on_open_callback=self.on_connection_open,
                                     on_open_error_callback=self.on_connection_open_error,
                                     on_close_callback=self.on_connection_closed)

    def on_connection_open(self, connection: pika.SelectConnection):

        self.logger.info('Connection opened')
        self.open_channel()

    def on_connection_open_error(self, connection: pika.SelectConnection, err: Exception):

        self.logger.critical('Connection open failed: %s', err)
        self._connection.ioloop.add_timeout(3, self.reconnect)

    def on_connection_closed(self, connection: pika.connection.Connection, reply_code, reply_text):

        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            self.logger.warning('Connection closed, reopening in 3 seconds: %s',
                                reply_text)
            self._connection.ioloop.add_timeout(3, self.reconnect)

    def reconnect(self):

        self._connection.ioloop.stop()

        if not self._closing:
            # Create a new connection
            self._connection = self.connect()

            # There is now a new connection, needs a new ioloop to run
            self._connection.ioloop.start()

    def open_channel(self):
        self.logger.info('Creating a new channel')
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        self.logger.info('Channel opened')
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.exchange)

    def add_on_channel_close_callback(self):
        self.logger.info('Adding channel close callback')
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel: pika, reply_code, reply_text):
        self.logger.warning('Channel %i was closed: %s', channel, reply_text)
        self._connection.close()

    def setup_exchange(self, exchange_name: str):
        self.logger.info('Declaring exchange: %s', exchange_name)
        # Note: using functools.partial is not required, it is demonstrating
        # how arbitrary data can be passed to the callback when it is called
        cb = functools.partial(self.on_exchange_declareok, userdata=exchange_name)

        self._channel.exchange_declare(
            exchange=exchange_name,
            callback=cb
        )

    def on_exchange_declareok(self, frame=None, userdata: str = ''):
        self.logger.info('Exchange declared: %s', userdata)

        for queue in self.queues:
            self.setup_queue(queue)

    def setup_queue(self, queue_name: str):
        self.logger.info('Declaring queue %s', queue_name)
        cb = functools.partial(self.on_queue_declareok,
                               userdata=queue_name)
        self._channel.queue_declare(queue=queue_name, callback=cb)

    def on_queue_declareok(self, method_frame, userdata: str):
        queue_name = userdata
        self.logger.info('Binding %s to %s',
                         self.exchange, queue_name)
        cb = functools.partial(self.on_bindok,
                               userdata=queue_name)
        self._channel.queue_bind(queue=queue_name,
                                 exchange=self.exchange,
                                 callback=cb)

    def on_bindok(self, frame, userdata: str):
        self.logger.info('Queue bound: %s', userdata)
        self.start()

    def start(self):
        """
        Start consuming or publishing.
        """
        pass

    def close_channel(self):
        self.logger.info('Closing the channel')
        self._channel.close()

    def close_connection(self):
        self.logger.info('Closing connection')
        self._connection.close()

    def stop(self):
        self.logger.info('Stopping')
        self._closing = True
        self.close_channel()
        self.close_connection()
        self.logger.info('Stopped')

    def run(self):
        self._connection = self.connect()
        self._connection.ioloop.start()
