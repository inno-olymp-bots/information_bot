import psycopg2
from . import settings as s


def tg_id_exists(user_id):
    # check if such tg_id exists in db
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM users WHERE tg_id = %s', (user_id,))
    results = cursor.fetchall()
    connection.close()
    return len(results) > 0


def get_state(user_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT state FROM users WHERE tg_id = %s', (user_id,))
    user = cursor.fetchall()
    connection.close()
    if len(user) > 0:
        return user[0][0]
    else:
        return 0


def update_state(tg_id, state):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'UPDATE users SET state={state} WHERE tg_id={tg_id}')
    connection.commit()
    connection.close()


def is_key_valid(text):
    try:
        key = text.split(" ")[1]
    except:
        return False
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f"SELECT secret_key FROM users_jurygroup WHERE secret_key='{key}'")
    data = cursor.fetchall()
    connection.close()
    try:
        data[0][0]
        return True
    except:
        return False


def register_chat(chat_id, key):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f"UPDATE users_jurygroup SET tg_id = {chat_id} WHERE secret_key='{key}'")
    connection.commit()
    connection.close()


def get_id_by_tg_id(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT id FROM users WHERE tg_id = %s', (tg_id,))
    result = cursor.fetchone()
    if result is not None:
        result = result[0]
    connection.commit()
    connection.close()
    return result


def get_user_info(user_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(
        'SELECT status, region_id FROM users WHERE id = %s',
        (user_id,))
    row = cursor.fetchone()
    result = {
        'status': row[0]
    }
    res = None
    if row[1] is not None:
        cursor.execute('SELECT name FROM users_region WHERE id = %s', (row[1],))
        res = cursor.fetchone()[0]
    result['region'] = res
    cursor.execute(
        'SELECT cat.name, comp.name FROM (competitions AS comp JOIN competition_categories AS cat '
        'ON comp.category_id = cat.id) JOIN users AS u ON u.competition_id = comp.id WHERE u.id = %s',
        (user_id,))
    row = cursor.fetchone()
    if row is not None:
        row = row[0] + ' - ' + row[1]
    result['competition'] = row
    connection.commit()
    connection.close()
    return result


def get_user_name(tg_id):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute('SELECT name FROM users WHERE tg_id = %s', (tg_id,))
    result = cursor.fetchone()
    if result is not None:
        result = result[0]
    connection.commit()
    connection.close()
    return result


def save_statistic(tg_id, action):
    connection = psycopg2.connect(s.DB_CONNECTION)
    cursor = connection.cursor()
    cursor.execute(f'SELECT status, token FROM users WHERE tg_id={tg_id}')
    user = cursor.fetchall()[0]
    status, token = user[0], user[1]
    has_token = True if (token != "") and (token is not None) else False
    cursor.execute(f'INSERT INTO users_statistic (tg_id, query, datetime, status, has_token) VALUES'
                   f' ({tg_id}, {action}, now(), {status}, {has_token})')
    connection.commit()
    connection.close()
