#################################################
#              Main settings file.              #
# Do not change ANYTHING without authorization. #
#################################################
import logging
from datetime import date

# TOKEN #

TOKEN: str = "616177821:AAFtsaybQqLkunVHbnaEQbjZdWi_SsEiJKA"

# PROXY #

PROXY_SETTINGS = {
    # 'https': 'socks5://freetelegram.org:freetelegram.org@srv1.freetelegram.org:10000'
}

# RABBITMQ #

RABBITMQ_HOST: str = 'rmq'
RABBITMQ_PORT: int = 5672
RABBITMQ_USERNAME = 'RMQ_USER'
RABBITMQ_PASSWORD = 'RMQ_PASSWORD'

# DATABASE #

DB_HOST = 'pg'
DB_NAME = 'DB'
DB_USER = 'DB_USER'
DB_PASSWORD = 'DB_PASSWORD'

DB_CONNECTION = f"host='{DB_HOST}' dbname='{DB_NAME}' user={DB_USER} password={DB_PASSWORD}"

# WEBHOOK #
# todo add webhook
WEBHOOK_HOST = 'WHERE_TO_POINT_WEBHOOK'
WEBHOOK_PORT = 443

WEBHOOK_HOST = 'WHERE_TO_HOST_WEBHOOK'
WEBHOOK_PORT = 8443

WEBHOOK_LISTEN = '0.0.0.0'

WEBHOOK_URL_BASE = f'https://{WEBHOOK_HOST}:{WEBHOOK_PORT}'
WEBHOOK_URL_PATH = f'/{TOKEN}/'

WEBHOOK_SSL_CERT = 'PATH_TO_CERT'
WEBHOOK_SSL_PRIV = 'PATH_TO_PRIVATE_KEY'


SHEETS_API_SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOG_LEVEL = logging.ERROR
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)


TIME_ZONE = 'Europe/Moscow'
FIRST_DAY_DATE = date(day=22, month=6, year=2018) # todo change

CHEATERS_FINDER_FLAG = True
