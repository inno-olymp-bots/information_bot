import pickle
from threading import Event, Thread
from time import sleep
from typing import List

from core.consumer import MessageQueueOperator


class Publisher(MessageQueueOperator):
    """
    This is an example publisher that will handle unexpected interactions
    with RabbitMQ such as channel and connection closures.

    If RabbitMQ closes the connection, it will reopen it. You should
    look at the output, as there are limited reasons why the connection may
    be closed, which usually are tied to permission related issues or
    socket timeouts.

    It uses delivery confirmations and illustrates one way to keep track of
    messages that have been sent and if they've been confirmed by RabbitMQ.
    """

    def __init__(self,
                 host: str,
                 port: int,
                 queues_names: List[str],
                 username: str = 'guest',
                 password: str = 'guest',
                 exchange: str = 'some_exchange',
                 logger_name: str = 'PublisherMQ'):
        """
        Create a new instance of the publisher class.
        :param host:
        :param port:
        :param queues_names:
        :param username:
        :param password:
        :param exchange:
        """
        super().__init__(host, port, username, password, queues_names, exchange, logger_name)

        self.queues_names = queues_names
        self._deliveries = None
        self._acked = None
        self._nacked = None
        self._message_number = None

        self._event: Event = None
        self._thread: Thread = None

    def start(self):
        self.logger.info('Issuing consumer related RPC commands')
        self.enable_delivery_confirmations()

    def enable_delivery_confirmations(self):
        self.logger.info('Issuing Confirm.Select RPC command')
        self._channel.confirm_delivery(callback=self.on_delivery_confirmation)

    def on_delivery_confirmation(self, method_frame):
        confirmation_type = method_frame.method.NAME.split('.')[1].lower()
        self.logger.info('Received %s for delivery tag: %i',
                         confirmation_type,
                         method_frame.method.delivery_tag)
        if confirmation_type == 'ack':
            self._acked += 1
        elif confirmation_type == 'nack':
            self._nacked += 1

        # HOT FIX
        try:
            self._deliveries.remove(method_frame.method.delivery_tag)
        except ValueError:
            pass

        self.logger.info('Published %i messages, %i have yet to be confirmed, '
                             '%i were acked and %i were nacked',
                             self._message_number, len(self._deliveries),
                             self._acked, self._nacked)

    def publish_message(self, message, queue: str):

        if self._channel is None or not self._channel.is_open:
            return

        if queue not in self.queues_names:
            raise ValueError(f'Queue {queue} is not defined.')

        self._channel.basic_publish(self.exchange, queue, message)

        self._message_number += 1
        self._deliveries.append(self._message_number)
        self.logger.info('Published message # %i', self._message_number)

    def _run(self, event: Event):

        while not self._closing:
            self._connection = None
            self._deliveries = []
            self._acked = 0
            self._nacked = 0
            self._message_number = 0

            if event.is_set():
                self._connection = self.connect()
                self._connection.ioloop.start()
            else:
                self.stop()
                if (self._connection is not None and
                        not self._connection.is_closed):
                    # Finish closing
                    self._connection.ioloop.start()

        self.logger.info('Stopped')

    def run(self):
        self._event = Event()
        self._event.set()

        self._thread = Thread(target=self._run, args=(self._event,))
        self._thread.start()

        sleep(3.0)

        self.logger.info('Started')

    def terminate(self):
        self.stop()
        self._event.clear()
        self._thread.join(timeout=10)


class PicklePublisher(Publisher):
    def publish_message(self, message, queue: str):
        super().publish_message(pickle.dumps(message, fix_imports=False), queue)


if __name__ == '__main__':

    try:
        pub = PicklePublisher('localhost', 5672, ['test'], exchange='info_bot')
        pub.run()

        while True:
            pub.publish_message('test', 'test')
            print('msg')
            sleep(1000)

    except KeyboardInterrupt:
        pub.terminate()
