#!/usr/bin/env bash
docker stack rm info_bots
echo "Wait"
sleep 2
docker container prune -f

./build-local.sh
docker stack deploy --compose-file docker_config/compose-local-microservices.yml info_bots
echo "Completed"
