# README
InfoBot is a Telegram Bot created for events such like olympiads and competitions held in Innopolis University. The project is developed by a group of students. InfoBot is used to register users, show information about competitions in comfortable way, provide user with organisational information and etc.

InfoBot consists of two parts:
- Bot Microservices
- Main Services

Here we review **Bot Microservices** part. To see **Main Services** part visit second project [repository][repo].

# Installation Process
Beforehand, you should go through the installation process of Bot Microservices. Finally, we suppose you have already started [PostgreSQL][postgres] and [RabbitMQ][rabbit] containers in Docker.
Go through several steps:
 - replace client_secret.json files in **table_microservice** folder with your own client_secret.json file that is your Google Sheets API key. Use this [quickstart][gog_api] to create your API key. Then, go through [credentials_generation](table_microservice/credentials_generation/credentials_generation.md) instruction to obtain the credentials.json file
 - replace the bot's token in **core.settings.py** file with your own bot's token (and other settings if needed).

The project contains of several separate microservices that should be run.
Run **build-local.sh** and **no-wait-local-server.sh** to build images and start all containers in Docker on localhost.

Use **prod-prod.sh** and **no-wait-prod-server.sh** for a server the same way (it uses webhook instead of polling).





[repo]: <https://gitlab.com/inno-olymp-bots/information_bot_web>
[docker]: <https://www.docker.com/>
[dock_comp]: <https://docs.docker.com/compose/>
[rabbit]: <https://www.rabbitmq.com/>
[postgres]: <https://www.postgresql.org/>
[gog_api]: <https://developers.google.com/sheets/api/quickstart/python>