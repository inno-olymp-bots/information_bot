#!/usr/bin/env bash
./build-local.sh
docker stack deploy --compose-file docker_config/compose-local-microservices.yml info_bots
echo "Completed"
