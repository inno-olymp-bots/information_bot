import time

import schedule
import telebot
from telebot import apihelper

from core.publisher import PicklePublisher
from core.settings import *
from schedule_microservice.database import *

# Proxy
apihelper.proxy = s.PROXY_SETTINGS

# Bot
bot = telebot.TeleBot(s.TOKEN, num_threads=4)

# Logger
logger = logging.getLogger(__name__)


try:
    pub = PicklePublisher(RABBITMQ_HOST, RABBITMQ_PORT, 'broadcast', RABBITMQ_USERNAME, RABBITMQ_PASSWORD,
                          exchange='info_bot', logger_name='broadcast')
except ValueError:
    logger.log(logging.CRITICAL, 'Could not create PicklePublisher!')


def job():
    ids = get_all_messages_by_now()
    for msg_id in ids:
        message = [0, msg_id]  # send to all
        pub.publish_message(
            message,
            'broadcast'
        )
    set_valid_all_messages(ids)

    ids = get_personal_messages_by_now()
    for msg_id in ids:
        message = [1, msg_id]  # send to person
        pub.publish_message(
            message,
            'broadcast'
        )
    set_valid_personal_messages(ids)

    ids = get_team_messages_by_now()
    for msg_id in ids:
        message = [2, msg_id]  # send to teams
        pub.publish_message(
            message,
            'broadcast'
        )
    set_valid_team_messages(ids)

    ids = get_competition_messages_by_now()
    for msg_id in ids:
        message = [3, msg_id]  # send by competitions
        pub.publish_message(
            message,
            'broadcast'
        )
    set_valid_competition_messages(ids)

    ids = get_region_messages_by_now()
    for msg_id in ids:
        message = [4, msg_id]  # send by regions
        pub.publish_message(
            message,
            'broadcast'
        )
    set_valid_region_messages(ids)

    ids = get_status_messages_by_now()
    for msg_id in ids:
        message = [5, msg_id]  # send by status
        pub.publish_message(
            message,
            'broadcast'
        )
    set_valid_status_messages(ids)


try:
    schedule.every(30).seconds.do(job)
except Exception as e:
    logger.log(logging.CRITICAL, 'Could not schedule job!')
    logger.log(logging.CRITICAL, e)


def non_stop():
    logger.log(logging.DEBUG, 'Schedule started pending!')
    while True:
        schedule.run_pending()
        time.sleep(1)


try:
    pub.run()
    logger.log(logging.DEBUG, 'PicklePublisher run successfully!')
except Exception as e:
    logger.log(logging.CRITICAL, 'Could not run PicklePublisher!')
    logger.log(logging.CRITICAL, e)

try:
    non_stop()
    raise KeyboardInterrupt
except KeyboardInterrupt:
    logger.log(logging.CRITICAL, 'Keyboard Interruption of non_stop()!')
    pub.terminate()
except Exception as e:
    logger.log(logging.CRITICAL, 'Schedule run failed!')
    logger.log(logging.CRITICAL, e)
